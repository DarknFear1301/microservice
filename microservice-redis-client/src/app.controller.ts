import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}


  @Post() 
  async add(@Body('data') data: number[]) {
    console.log('data: ', data);
    const sum = await this.appService.accumulate(data);
    console.log('sum: ', sum);
  }
 
}
