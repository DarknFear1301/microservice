import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, MessagePattern, Transport } from '@nestjs/microservices';

@Injectable()
export class AppService {
  constructor(@Inject('CLIENT_SERVICE') private client: ClientProxy) {  
  }
  
  async accumulate(data: number[]): Promise<any> {
    console.log('debug1:');
    const sum = await this.client.send({cmd: 'add'},  data).toPromise();
    console.log('debug:', sum);
    return await this.client.send({cmd: 'add'},  data).toPromise();
  }
}
