import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const microservice = app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.RMQ,
    options: {
      urls: ['amqp://localhost:5672'],
      queue: 'queue',
      queueOptions: {
        durable: false
      },
    }
  });
  await app.startAllMicroservices();

  const config = new DocumentBuilder()
    .setTitle('Demo example')
    .setDescription('The demo API description')
    .setVersion('1.0')
    .addTag('demo')
    .build();

  const document =  SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000);
}
bootstrap();
