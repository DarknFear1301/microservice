import { Injectable } from '@nestjs/common';
@Injectable()
export class AppService {
  constructor() {}

  
  add(data: number[]): number {
    return data.reduce((sum, item) => item + sum, 0);
  }
}
