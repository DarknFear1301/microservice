import { Body, Controller, Get, Post } from '@nestjs/common';
import { Ctx, MessagePattern, Payload, RedisContext } from '@nestjs/microservices';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}


  @MessagePattern({ cmd: 'add'})
  add(@Payload() data: number[], @Ctx() context: RedisContext) {
    console.log('payload: ', data);
    console.log('context: ', context)
    return this.appService.add(data);
  }
  
}
